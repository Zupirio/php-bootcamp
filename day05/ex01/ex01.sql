LOAD DATA LOCAL INFILE './base-students.sql' INTO TABLE `ex01`.`db_arangari.ft_table`;

CREATE TABLE `db_arangari` . `ft_table`
( `id` INT NOT NULL AUTO_INCREMENT ,
`login` VARCHAR(50) NOT NULL DEFAULT 'toto' ,
`group` ENUM('staff', 'student', 'other') NOT NULL ,
`creation_date` DATE NOT NULL ,
PRIMARY KEY (`id`)
);